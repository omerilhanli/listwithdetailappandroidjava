package com.android.listdetailapp.UI;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.listdetailapp.R;
import com.android.listdetailapp.UI.adapter.RecyclerAdapter;
import com.android.listdetailapp.callback.RowClickListener;
import com.android.listdetailapp.model.User;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private List<User> users;

    private RowClickListener listener = new RowClickListener() {
        @Override
        public void onClicked(int position) {

            User selectedUser = users.get(position);

            DetailActivity.startActivityFrom(MainActivity.this, selectedUser);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {

        prepareToolbar();

        fbyId();

        create();

        prepareThenLoad();
    }

    private void prepareToolbar() {

        getSupportActionBar().setTitle(getClass().getSimpleName());
    }

    private void fbyId() {

        recyclerView = findViewById(R.id.recyclerView);
    }

    private void prepareThenLoad() {

        RecyclerAdapter adapter = new RecyclerAdapter(users, listener);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    private void create() {

        User user01 = new User(getString(R.string.txt_artiz_1), R.drawable.d1_cmylmz);
        User user02 = new User(getString(R.string.txt_artiz_2), R.drawable.d2_knbylgn);
        User user03 = new User(getString(R.string.txt_artiz_3), R.drawable.d3_byztztrk);
        User user04 = new User(getString(R.string.txt_artiz_4), R.drawable.d4_ymzrdgn);

        users
                = Arrays.asList(user01, user02, user03, user04);
    }

}
