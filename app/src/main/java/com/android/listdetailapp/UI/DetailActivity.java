package com.android.listdetailapp.UI;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.listdetailapp.common.BundleAttribute;
import com.android.listdetailapp.R;
import com.android.listdetailapp.model.User;

public class DetailActivity extends AppCompatActivity {

    private ImageView imgVPhoto;

    private TextView txtVName;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                onBackPressed();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() { // initialization for all objects.

        prepareToolbar();

        fbyId();

        get();

        bind();
    }

    private void prepareToolbar() {

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setDisplayShowHomeEnabled(true);


        actionBar.setTitle(getClass().getSimpleName());
    }

    private void fbyId() {

        imgVPhoto = findViewById(R.id.imgVPhoto);

        txtVName = findViewById(R.id.txtVName);
    }

    private void get() { // get user from bundle

        Bundle bundle = getIntent().getExtras();

        if (bundle == null) return;

        user = (User) bundle.getSerializable(BundleAttribute.KEY_USER);
    }

    private void bind() { // user bind to view

        if (user == null) return;

        imgVPhoto.setImageResource(user.getUserPhotoDrawable());

        txtVName.setText(user.getUserName());
    }

    public static void startActivityFrom(Activity activity, User user) {

        Intent intent = new Intent(activity, DetailActivity.class);

        intent.putExtra(BundleAttribute.KEY_USER, user);

        activity.startActivity(intent);
    }

}
