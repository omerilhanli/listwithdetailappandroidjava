package com.android.listdetailapp.UI.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.listdetailapp.R;
import com.android.listdetailapp.callback.RowClickListener;
import com.android.listdetailapp.model.User;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.VH> {

    private List<User> userList;

    private RowClickListener rowClickListener;

    public RecyclerAdapter(List<User> userList, RowClickListener rowClickListener){

        this.userList=userList;

        this.rowClickListener=rowClickListener;
    }

    @NonNull
    @Override
    public RecyclerAdapter.VH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);

        return new VH(view, rowClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.VH vh, int position) {

        User user = userList.get(position);

        vh.imgVPhoto.setImageResource(user.getUserPhotoDrawable());

        vh.txtVName.setText(user.getUserName());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class VH extends RecyclerView.ViewHolder {

        ImageView imgVPhoto;
        TextView txtVName;

        VH(@NonNull View itemView, final RowClickListener rowClickListener) {
            super(itemView);

            fbyId(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    rowClickListener.onClicked(getAdapterPosition());
                }
            });
        }

        void fbyId(View view){

            imgVPhoto = view.findViewById(R.id.imgVPhoto);

            txtVName = view.findViewById(R.id.txtVName);
        }
    }
}
