package com.android.listdetailapp.model;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = -1604145567745986482L;

    private String userName;
    private int userPhotoDrawable;

    public User(String userName, int userPhotoDrawable) {
        this.userName = userName;
        this.userPhotoDrawable = userPhotoDrawable;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserPhotoDrawable() {
        return userPhotoDrawable;
    }

    public void setUserPhotoDrawable(int userPhotoDrawable) {
        this.userPhotoDrawable = userPhotoDrawable;
    }
}
