package com.android.listdetailapp.callback;


public interface RowClickListener {

    void onClicked(int position);
}
